-- chapter1-5-2
-- local definition
f :: (Float, Float) -> Float
f(x,y) = (a + 1) * (a + 2) where a = (x + y) / 2

f2 :: (Float, Float) -> Float
f2 (x, y) = (a + 1) * (b + 2)
            where a = (x + y) / 2
                  b = (x + y) / 3

f3 :: (Float, Float) -> Float
f3 (x, y) = (a + 1) * (b + 2)
            where a = (x + y) / 2; b = (x + y) / 3

square :: Integer -> Integer
square x = x * x

f4 :: Integer -> Integer -> Integer
f4 x y
    | x <= 10 = x + a
    | x > 10  = x - a
    where  a = square(y + 1)

main = do
    print("chapter1-5-2")
    print $ f(8,9)
    print $ f2(8,9)
    print $ f3(8,9)
    print $ f4 10 11
