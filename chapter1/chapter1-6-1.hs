-- chapter1-6-1
square :: Integer -> Integer
sqrt2 :: Integer -> Float

square x = x * x
sqrt2 x = (realToFrac(x::Integer))

{-- (.) :: (β -> γ) -> (α -> β) -> (α -> γ)
 - なのでsqrt . square は以下のようになる
 - sqrt . square :: Integer -> Float
 -               :: (Integer -> Float) -> (Integer -> Integer) -> (Integer -> Float)
 - (.)の定義のα,β,γを型変数と呼び、異なる状況では異なる型に具体化される
 - 同様にerrorの方は String -> αとなる
 --}
main = do
    print("chapter1-6-1")
    print $ (sqrt2 . square ) 2
