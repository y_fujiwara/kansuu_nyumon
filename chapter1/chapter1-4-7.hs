-- chapter1-4-7 関数合成
square :: Integer -> Integer
square x = x * x

quad :: Integer -> Integer
-- ポイントフリースタイル 引数xとかのことをポイントと呼ぶ
quad = square . square

quad2 :: Integer -> Integer
-- ホワイトワイズスタイル
quad2 x = square (square x)

main = do
    print("chapter1-4-7")
    print $ quad 2
    print $ quad2 2
