{-- chapter 1.4.1 
 -The principle of the extension of
 --}
double, double2 :: Integer -> Integer
double x = x + x
double2 x = 2 * x

main = do
    print("chapter1.4.1")
    print $ double 2
    print $ double2 2
    print("double = double2")
