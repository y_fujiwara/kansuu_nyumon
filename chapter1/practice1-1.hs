square :: Float -> Float
square x = x*x

-- 1.1.1
quad :: Float -> Float
quad x = square(x) * square(x)

-- 1.1.2
greater :: (Integer,Integer) -> Integer
greater (x,y) = if x >= y then x else y

-- 1.1.3
area :: Float -> Float
area x = square(x) * (22.0/7.0)

main = do print("p4 Practice")
          print $ quad 3
          print $ greater(8,10)
          print $ area 3
