-- chapter1-5-1
-- recursive function
fact :: Integer -> Integer
fact n = if n == 0 then 1 else n * fact(n-1)

-- 上記野定義は-1などが入ると止まらなくなる
-- 停止させるためにGuardを使う
fact2 :: Integer -> Integer
-- errorは文字列を取り、評価されると直ちに評価器を停止する関数
fact2 n
    | n < 0  = error "negative argument to fact"
    | n == 0 = 1
    | n > 0  = n * fact(n-1)

main = do 
    print("chapter1-5-1")
    print $ fact 10
    --print $ fact2(-1)
    print $ fact2 0
    print $ fact2 10
