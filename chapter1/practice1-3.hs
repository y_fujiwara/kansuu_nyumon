multiply :: (Integer,Integer) -> Integer
multiply(x,y) = if x == 0 then 0 else x * y

infinity :: Integer
infinity = infinity + 1

main = do print("practice1-3")
          print $ multiply(0, infinity)
          print $ multiply(infinity,0) -- infinity * 0 is infinity roop
