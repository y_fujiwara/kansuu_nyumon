-- 1.6.1
const :: a -> b -> a
const x y = x

subst :: (a -> b -> c) -> (a -> b) -> a -> c
subst f g x = f x (g x)

apply :: (a -> b) -> a -> b
apply f x = f x

flip :: (a -> b -> c) -> b -> a -> c
flip f x y = f y x

square :: Integer -> Integer
square x = x * x

temp :: Integer -> Integer -> Integer
temp x y = x + y

main = do
    print("practice1-6")
    print $ Main.const 2 3
    print $ subst Main.const square 2
    print $ Main.apply square 2
    print $ Main.flip temp 3 2

