-- 関数定義
square :: Integer -> Integer
square x = x*x


smaller :: (Integer,Integer) -> Integer
smaller (x,y) = if x <= y then x else y

square2 :: Float -> Float
square2 x = x*x

delta :: (Float,Float,Float) -> Float
delta (a,b,c) = sqrt(square2 b-4*a*c)

main = do print("p2,p3 Script")
          print $ square 3786
          print $ square( smaller(5,4+4))
          print $ delta(4.2,7,2.3)
