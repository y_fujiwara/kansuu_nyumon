-- practice1-4
-- 1.4.1
f :: Integer -> Integer 
g :: Integer -> (Integer -> Integer)

f x = x * x
g x y = x * y 

h :: Integer -> Integer -> Integer
h x y = f(g x y)
{--
 - 関数合成は右の関数の戻り値と左の関数の引数が一致して無くてはならない
 - func :: a -> b
 - func2 :: b -> c
 - func3 :: a -> c
 - func3 = func2 . func
 - よって　h x = f . (g x)
 --}

-- 1.4.2
square2 :: Float -> Float
square2 x = x*x

delta :: Float -> Float -> Float -> Float
delta a b c = sqrt(square2 b-4*a*c)

-- 1.4.3
-- 第一引数として底を取り、第二引数として値を取ると考えれば
-- log :: Integer -> Float -> ///

-- 1.4.4
-- 範囲と関数を引数に取ると考えれば良い
-- Float -> Float -> (Float -> Float) -> Float

-- 1.4.5
f2 :: (Integer -> Integer) -> Integer
f3 :: (Integer -> Integer) -> (Integer -> Integer)
f2 x = (x . x) 2
f3 x = x . x

-- 1.4.6


-- 1.4.7
uncurry2 :: (a -> b -> c) -> ((a,b) -> c)
uncurry2 g (x, y) = g x y

main = do
    print("practice1-4")
    print $ h 2 4
    print $ delta 4.2 7 2.3
    print $ f2 f
    print $ f3 f 2
  
