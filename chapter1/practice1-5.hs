-- 1.5.1
fib :: Integer -> Integer
fib n
    | n == 0 = 0
    | n == 1 = 1
    | n > 1  = fib(n - 1) + fib(n - 2)
    | n < 0  = error "negative argument error"

abs2 :: Integer -> Integer
abs2 x 
    | x < 0  = (-1) * x
    | x >= 0 = x

main = do
    print("practice1-5")
    print $ fib 3
    print $ abs2(100)
