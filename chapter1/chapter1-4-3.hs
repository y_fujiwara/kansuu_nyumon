plusc :: Integer -> Integer -> Integer
-- operator is function of infix notation
-- plusc = (+)
plusc x y = (+) x y -- x + y

main = do
    print("chapter1-4-3")
    print $ plusc 2 3
