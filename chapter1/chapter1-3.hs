three :: Integer -> Integer
three x = 3

square :: Integer -> Integer
square x = x*x

infinity :: Integer
infinity = infinity + 1

main = do print("chapter1-3")
          print $ three infinity
          print $ square infinity -- infinity roop

