{-- chapter1.4.2
 - Curry
 --}
square :: Integer -> Integer
square x = x * x

smaller :: (Integer, Integer) -> Integer
smaller (x, y) = if x <= y then x else y

smallerc :: Integer -> (Integer -> Integer)
smallerc x y = if x <= y then x else y

plus :: (Integer, Integer) -> Integer
plus (x, y) = x + y

plusc :: Integer -> (Integer -> Integer)
plusc x y = x + y

-- 関数を引数に取って関数を返す関数
-- 引数の関数を二回適用する関数
-- この例はカリー化によって部分適用が楽になるサンプル?
twice :: (Integer -> Integer) -> (Integer -> Integer)
twice f x = f (f x)

quad :: Integer -> Integer
quad = twice square

main = do
    print("chapter1-4-2")
    print $ smaller(2,3)
    print $ smallerc 2 3
    print $ plus(2,3)
    print $ (plusc 2) 3
    print $ quad 2
