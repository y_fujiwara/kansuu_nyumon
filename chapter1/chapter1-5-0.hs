-- chapter1-5
-- Guard
pi :: Float
pi = 3.141592

smaller :: (Integer, Integer) -> Integer
smaller (x, y)
    | x <= y = x
    | x > y =y

signum2 :: Integer -> Integer
signum2 x
    | x < 0  = -1
    | x == 0 = 0
    | x > 0  = 1

signum3 :: Integer -> Integer
signum3 x = if x < 0 then -1 else
            if x == 0 then 0 else 1

main = do
    print("chapter1-5")
    print $ smaller (2,4)
    print $ signum2 (-100)
    print $ signum3 (-90)
