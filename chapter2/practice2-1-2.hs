agree :: Bool -> Bool -> Bool
agree x y = if x then (if y then True else False) else True

main = do print("2.1.2 Script")
          print $ agree True False
          print $ agree True True
          print $ agree False False
          print $ agree False True