leapyear :: Int -> Bool
leapyear y = (y `mod` 4 == 0) && ((y `mod` 100 /= 0) || (y `mod` 400 == 0))

main = do print("leapyear Script")
          print $ leapyear 3786
          print $ leapyear 2000
          print $ leapyear 100
