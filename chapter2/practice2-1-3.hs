class Eq2 a where
    (/=) :: a -> a -> Bool
    x /= x = not (x == x)


main = do print("2.1.1 Script")
          print $ hat True True
          print $ hat True False
          print $ hat False True
          print $ hat False False
          print $ v True True
          print $ v True False
          print $ v False True
          print $ v False False
