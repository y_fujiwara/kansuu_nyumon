data Triangle = Failure | Isosceles | Equilateral | Scalene deriving Show

analyse :: (Int, Int, Int) -> Triangle
analyse (x, y, z)
    | (x + y) <= z = Failure
    | x == z = Equilateral
    | (x == y) || (y == z) = Isosceles
    | otherwise = Scalene

main = do print("snalyse Script")
          print $ analyse (1, 1, 1)
          print $ analyse ( 3, 4, 1)
          print $ analyse (1, 1, 2)
