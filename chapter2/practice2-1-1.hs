v :: Bool -> Bool -> Bool
v a b = if a then True else (if b then True else False)

hat :: Bool -> Bool -> Bool
hat a b = if a then (if b then True else False) else False

main = do print("2.1.1 Script")
          print $ hat True True
          print $ hat True False
          print $ hat False True
          print $ hat False False
          print $ v True True
          print $ v True False
          print $ v False True
          print $ v False False
